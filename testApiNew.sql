USE [master]
GO
/****** Object:  Database [TestApi]    Script Date: 22.08.2018 20:23:51 ******/
CREATE DATABASE [TestApi]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TestApi', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\TestApi.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'TestApi_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\TestApi_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [TestApi] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TestApi].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TestApi] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TestApi] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TestApi] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TestApi] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TestApi] SET ARITHABORT OFF 
GO
ALTER DATABASE [TestApi] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TestApi] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TestApi] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TestApi] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TestApi] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TestApi] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TestApi] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TestApi] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TestApi] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TestApi] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TestApi] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TestApi] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TestApi] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TestApi] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TestApi] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TestApi] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TestApi] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TestApi] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TestApi] SET  MULTI_USER 
GO
ALTER DATABASE [TestApi] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TestApi] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TestApi] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TestApi] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [TestApi] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [TestApi] SET QUERY_STORE = OFF
GO
USE [TestApi]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [TestApi]
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 22.08.2018 20:23:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](250) NOT NULL,
	[Phone] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[Hobby] [nvarchar](250) NULL,
	[AccessToken] [nvarchar](250) NOT NULL,
	[Age] [int] NOT NULL,
	[SuperUser] [bit] NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Contact] ON 

INSERT [dbo].[Contact] ([Id], [Email], [Phone], [Name], [Hobby], [AccessToken], [Age], [SuperUser]) VALUES (1, N'superUser', N'superUser', N'superUser', N'superUser', N'5F8D275C-356F-48F0-A4B0-F546BC192010', 0, 1)
INSERT [dbo].[Contact] ([Id], [Email], [Phone], [Name], [Hobby], [AccessToken], [Age], [SuperUser]) VALUES (4, N'alex_borik@mail.by', N'+375257878789', N'dfsdsf', N'dfdsf', N'5f220648-2c26-4ba7-b115-28665209ebf7', 23, 0)
SET IDENTITY_INSERT [dbo].[Contact] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [AK_Token]    Script Date: 22.08.2018 20:23:51 ******/
ALTER TABLE [dbo].[Contact] ADD  CONSTRAINT [AK_Token] UNIQUE NONCLUSTERED 
(
	[AccessToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [AK_UniqueUser]    Script Date: 22.08.2018 20:23:51 ******/
ALTER TABLE [dbo].[Contact] ADD  CONSTRAINT [AK_UniqueUser] UNIQUE NONCLUSTERED 
(
	[Email] ASC,
	[Phone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[addContact]    Script Date: 22.08.2018 20:23:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[addContact] 
@Token nvarchar(250),
@Email nvarchar(250),
@Phone nvarchar(50),
@Name nvarchar(250),
@Hobby nvarchar(250),
@Age int
AS
BEGIN
if @Email like '%_@__%.__%' and @Phone like '+375%' and LEN(@Phone)=13 and @Name IS NOT NULL and @Name != '' and @Age>17 and @Age<100 and @Token is not null and TRY_CAST(@Token AS UNIQUEIDENTIFIER) IS NOT NULL
   begin
INSERT INTO Contact(
    Email,
    Phone,
    [Name],
    Hobby,
    AccessToken,
    Age,
    SuperUser
) VALUES (
    @Email,
	@Phone,
	@Name,
	@Hobby,
	@Token,
	@Age,
	0
)
end

else return 0;
END
GO
/****** Object:  StoredProcedure [dbo].[deleteContact]    Script Date: 22.08.2018 20:23:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[deleteContact] @Token nvarchar(250), @Id int
AS

DECLARE @Contacts nvarchar(MAX) 
SET @Contacts = NULL

SELECT @Contacts = [dbo].[Contact].[AccessToken]
FROM Contact 
WHERE (Contact.AccessToken = @Token AND Contact.SuperUser = 1)

IF(@Contacts IS NULL or @Contacts = '') DELETE Contact where 1 = 2
Else DELETE Contact where Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[editContact]    Script Date: 22.08.2018 20:23:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery3.sql|7|0|C:\Users\boris\AppData\Local\Temp\~vs4E8.sql
CREATE PROCEDURE [dbo].[editContact] 
@Id int,
@Token nvarchar(250),
@Email nvarchar(250),
@Phone nvarchar(50),
@Name nvarchar(250),
@Hobby nvarchar(250),
@Age int
AS
BEGIN

   if @Email like '%_@__%.__%' and @Phone like '+375%' and LEN(@Phone)=13 and @Name IS NOT NULL and @Name != '' and @Age>17 and @Age<100
   begin
   UPDATE Contact
    SET Email=@Email, 
        Phone=@Phone, 
        [Name]=@Name,
		Hobby = @Hobby,
		Age = @Age
    WHERE (Contact.AccessToken = @Token AND Contact.Id = @Id) or (Contact.AccessToken = @Token AND Contact.SuperUser = 1)
   end
   else return 0;
    
END
GO
/****** Object:  StoredProcedure [dbo].[getContactById]    Script Date: 22.08.2018 20:23:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[getContactById] @Token nvarchar(250), @Id int
AS

DECLARE @Contacts nvarchar(MAX) 
SET @Contacts = NULL

SELECT @Contacts = [dbo].[Contact].[AccessToken]
FROM Contact 
WHERE (Contact.AccessToken = @Token AND Contact.Id = @Id) or (Contact.AccessToken = @Token AND Contact.SuperUser = 1)

IF(@Contacts IS NULL or @Contacts = '') SELECT * from Contact where 1=2
Else SELECT * FROM Contact  where Contact.Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[getContacts]    Script Date: 22.08.2018 20:23:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getContacts] @Token nvarchar(250)
AS

DECLARE @Contacts nvarchar(MAX) 
SET @Contacts = NULL

SELECT @Contacts = [dbo].[Contact].[AccessToken]
FROM Contact 
WHERE Contact.AccessToken = @Token AND Contact.SuperUser = 1

IF(@Contacts IS NULL or @Contacts = '') SELECT * from Contact where Contact.AccessToken = @Token
Else SELECT * FROM Contact 

GO
USE [master]
GO
ALTER DATABASE [TestApi] SET  READ_WRITE 
GO
