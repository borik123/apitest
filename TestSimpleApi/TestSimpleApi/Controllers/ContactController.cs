﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestSimpleApi.Attributes;
using TestSimpleApi.Data;
using TestSimpleApi.Models;

namespace TestSimpleApi.Controllers
{
    [TokenAuthenticationAttribute]
    public class ContactController : ApiController
    {
        private readonly ContactRepository contactRepository = new ContactRepository(new System.Data.Entity.DbContext(
            @"Data Source = DESKTOP-CEPL3L1\SQLEXPRESS;Initial Catalog=TestApi;Integrated Security=True"));
        // GET api/values
        [HttpGet]
        public IEnumerable<Contact> Get()
        {
            Guid guid = Guid.Parse(Request.Headers.GetValues("Token").FirstOrDefault());
            try
            {
                var contacts = contactRepository.GetContacts(guid);
                return contacts;
            }
            catch (Exception)
            {

                return null;
            }
            
        }

        // GET api/contact/5
        [HttpGet]
        public Contact Get(int id)
        {
            Guid guid = Guid.Parse(Request.Headers.GetValues("Token").FirstOrDefault());
            try
            {
                var contact = contactRepository.GetContactById(guid, id);
                return contact;
            }
            catch (Exception)
            {

                return null;
            }
            
        }

        // POST api/contact
        [HttpPost]
        public HttpResponseMessage Post([FromBody]Contact value)
        {
            value.AccessToken = Guid.NewGuid().ToString();
            
            try
            {
                var result = contactRepository.AddContact(value);
                return result ? GetOkResponse("Contact added") : GetErrorResposeMessage("Post method error");
            }
            catch (Exception)
            {
                return GetErrorResposeMessage("Post method error");
            }

        }

        // PUT api/contact/5
        [HttpPut]
        public HttpResponseMessage Put(int id, [FromBody]Contact value)
        {
            value.AccessToken = Request.Headers.GetValues("Token").FirstOrDefault();
            value.Id = id;
            
            try
            {
                var result = contactRepository.EditContact(value);
                return result ?  GetOkResponse("Contact updated") : GetErrorResposeMessage("Put method error");
            }
            catch (Exception)
            {
                return GetErrorResposeMessage("Put method error");
            }
        }

        // DELETE api/contact/5
        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var result = contactRepository.DeleteContact(new Contact() { Id = id, AccessToken = Request.Headers.GetValues("Token").FirstOrDefault() });
                return result ? GetOkResponse("Contact deleted") : GetErrorResposeMessage("Delete method error");
            }
            catch (Exception)
            {
                return GetErrorResposeMessage("Delete method error");
            }
        }

        private HttpResponseMessage GetErrorResposeMessage(string message)
        {
            var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            response.Content = new StringContent(message);
            return response;
        }

        private HttpResponseMessage GetOkResponse(string message)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(message);
            return response;
        }
    }
}
