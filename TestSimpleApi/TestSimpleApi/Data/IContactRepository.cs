﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestSimpleApi.Models;

namespace TestSimpleApi.Data
{
    interface IContactRepository
    {
        IEnumerable<Contact> GetContacts(Guid token);
        Contact GetContactById(Guid token, int id);
        bool AddContact(Contact contact);
        bool EditContact(Contact contact);
        bool DeleteContact(Contact contact);
    }
}