﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TestSimpleApi.Data
{
    public abstract class Repository<TEntity> where TEntity : new()
    {
        DbContext _context;

        public Repository(DbContext context)
        {
            _context = context;
        }

        protected DbContext Context
        {
            get
            {
                return this._context;
            }
        }

        protected bool NonQuery(IDbCommand command, IDbConnection conn)
        {
            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            var result = command.ExecuteNonQuery();
            conn.Close();
            if (result >= 1)
                return true;
            else
                return false;
          
        }

        protected IEnumerable<TEntity> ToList(IDbCommand command, IDbConnection conn)
        {
            
            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();
            using (var record = command.ExecuteReader())
            {
                List<TEntity> items = new List<TEntity>();
                while (record.Read())
                {

                    items.Add(Map<TEntity>(record));
                }
                conn.Close();
                return items;
            }
            
        }

        protected TEntity Map<TEntity>(IDataRecord record)
        {
            var objT = Activator.CreateInstance<TEntity>();
            foreach (var property in typeof(TEntity).GetProperties())
            {
                if (!record.IsDBNull(record.GetOrdinal(property.Name)))
                    property.SetValue(objT, record[property.Name]);
            }
            return objT;
        }
    }
}