﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TestSimpleApi.Models;

namespace TestSimpleApi.Data
{
    public class ContactRepository: Repository<Contact>, IContactRepository
    {
        private DbContext _context;
       
        public ContactRepository(DbContext context)
            : base(context)
        {
            _context = context;
        }

        public IEnumerable<Contact> GetContacts(Guid token)
        {
            var conn = _context.Database.Connection;
            using (var command = conn.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "getContacts";
                command.Parameters.Add(GetDbParameter(command, DbType.String, "@Token", token.ToString()));
                return this.ToList(command,conn);
            }
        }


        public Contact GetContactById(Guid token, int id)
        {
            var conn = _context.Database.Connection;
            using (var command = conn.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "getContactById";
                command.Parameters.Add(GetDbParameter(command, DbType.String, "@Token", token.ToString()));
                command.Parameters.Add(GetDbParameter(command, DbType.Int32, "@Id", id));
                return this.ToList(command, conn).FirstOrDefault();
            }
        }

        public bool AddContact(Contact contact)
        {
            var conn = _context.Database.Connection;
            using (var command = conn.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "addContact";
                command.Parameters.Add(GetDbParameter(command, DbType.String, "@Token", contact?.AccessToken));
                command.Parameters.Add(GetDbParameter(command, DbType.String, "@Email", contact?.Email));
                command.Parameters.Add(GetDbParameter(command, DbType.String, "@Phone", contact?.Phone));
                command.Parameters.Add(GetDbParameter(command, DbType.String, "@Name", contact?.Name));
                command.Parameters.Add(GetDbParameter(command, DbType.String, "@Hobby", contact?.Hobby));
                command.Parameters.Add(GetDbParameter(command, DbType.Int32, "@Age", contact.Age));
                return this.NonQuery(command, conn);
            }
        }

        private DbParameter GetDbParameter(DbCommand command ,DbType type, string paramName, object value)
        {
            DbParameter parameter = command.CreateParameter();
            parameter.DbType = type;
            parameter.ParameterName = paramName;
            if (value != null)
            {
                parameter.Value = value;
            }
            else
            {
                parameter.Value = DBNull.Value;
            }
            
            return parameter;
        }

        public bool EditContact(Contact contact)
        {
            var conn = _context.Database.Connection;
            using (var command = conn.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "editContact";
                command.Parameters.Add(GetDbParameter(command, DbType.Int32, "@Id", contact.Id));
                command.Parameters.Add(GetDbParameter(command, DbType.String, "@Token", contact?.AccessToken));
                command.Parameters.Add(GetDbParameter(command, DbType.String, "@Email", contact?.Email));
                command.Parameters.Add(GetDbParameter(command, DbType.String, "@Phone", contact?.Phone));
                command.Parameters.Add(GetDbParameter(command, DbType.String, "@Name", contact?.Name));
                command.Parameters.Add(GetDbParameter(command, DbType.String, "@Hobby", contact?.Hobby));
                command.Parameters.Add(GetDbParameter(command, DbType.Int32, "@Age", contact.Age));
                return this.NonQuery(command, conn);
            }
        }

        public bool DeleteContact(Contact contact)
        {
            var conn = _context.Database.Connection;
            using (var command = conn.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "deleteContact";

                command.Parameters.Add(GetDbParameter(command, DbType.String, "@Token", contact?.AccessToken.ToString()));
                command.Parameters.Add(GetDbParameter(command, DbType.Int32, "@Id", contact.Id));
                return this.NonQuery(command, conn);
            }
        }
    }
}