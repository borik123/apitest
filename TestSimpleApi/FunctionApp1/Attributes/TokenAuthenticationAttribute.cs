﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Filters;

namespace FunctionApp1.Attributes
{
    public class TokenAuthenticationAttribute : Attribute, IAuthenticationFilter
    {
        public TokenAuthenticationAttribute()
        {

        }

        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)

        {
            var req = context.Request;

            IEnumerable<string> headerValues;

            var tokenHeaderValue = req.Headers.TryGetValues("Token", out headerValues);


            if (tokenHeaderValue == true && headerValues != null && headerValues.Count() > 0)
            {
                var token = headerValues.FirstOrDefault();

                Guid guid;
                bool isValid = Guid.TryParse(token, out guid);
                if (isValid && guid != null)
                {
                    context.ActionContext.Response = new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                {
                    context.ActionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                }

            }
            else
            {
                context.ActionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);
            }


            return Task.FromResult(0);
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {

            if (context.ActionContext.Response.StatusCode != HttpStatusCode.BadRequest)
            {
                context.Result = new HttpOkResult(context.Result);
            }
            else
            {
                context.Result = new ForbiddenMessageResult("access denied");
            }


            return Task.FromResult(0);
        }

        public bool AllowMultiple
        {
            get { return false; }
        }
    }


    public class ForbiddenMessageResult : IHttpActionResult
    {
        private string message;

        public ForbiddenMessageResult(string message)
        {
            this.message = message;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
            response.Content = new StringContent(message);
            return Task.FromResult(response);
        }
    }

    public class HttpOkResult : IHttpActionResult
    {
        private readonly IHttpActionResult next;


        public HttpOkResult(IHttpActionResult next)
        {
            this.next = next;
        }

        public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {

            var response = await next.ExecuteAsync(cancellationToken);


            return response;
        }
    }
}
