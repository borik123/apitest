using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using FunctionAppTestApi.Attributes;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using TestSimpleApi.Data;
using TestSimpleApi.Models;

namespace FunctionAppTestApi
{
   
    public static class AzureFunctionTest
    {
        private readonly static ContactRepository contactRepository = new ContactRepository(new System.Data.Entity.DbContext(@"Data Source = DESKTOP-CEPL3L1\SQLEXPRESS;Initial Catalog=TestApi;Integrated Security=True"));
      
        [FunctionName("Get")]
        [TokenAuthenticationAttribute]
        public static async Task<HttpResponseMessage> Get([HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            req.Properties[System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey] = new HttpConfiguration();

            if (HasGuidHeader(req))
            {
                Guid guid = Guid.Parse(req.Headers.GetValues("Token").FirstOrDefault());
                try
                {
                    var contacts = contactRepository.GetContacts(guid);
                    return req.CreateResponse(HttpStatusCode.OK, contacts);

                }
                catch (Exception)
                {

                    return req.CreateResponse(HttpStatusCode.Forbidden, "�"); ;
                }
            }
            else
            {
                return req.CreateResponse(HttpStatusCode.Forbidden, "�");
            }
        }


        [FunctionName("GetById")]
        [TokenAuthenticationAttribute]
        public static async Task<HttpResponseMessage> GetById([HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            req.Properties[System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey] = new HttpConfiguration();
            int id;
            bool idParam = int.TryParse(req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "id", true) == 0)
                .Value,out id);

            if (HasGuidHeader(req) && idParam)
            {
                Guid guid = Guid.Parse(req.Headers.GetValues("Token").FirstOrDefault());
                try
                {
                    var contact = contactRepository.GetContactById(guid, id);
                    
                    return req.CreateResponse(HttpStatusCode.OK, contact);

                }
                catch (Exception)
                {

                    return req.CreateResponse(HttpStatusCode.Forbidden, "�"); ;
                }
            }
            else
            {
                return req.CreateResponse(HttpStatusCode.Forbidden, "�");
            }
        }

        private static string GetDocumentContents(HttpRequestMessage request)
        {
            string str = string.Empty ;
            if (request.Content != null)
            {
                request.Content.ReadAsByteArrayAsync().ContinueWith
                    (
                        (task) =>
                        {

                            str = System.Text.UTF8Encoding.UTF8.GetString(task.Result);
                            
                        });
            }

            return str;
        }


        [FunctionName("Add")]
        [TokenAuthenticationAttribute]
        public static async Task<HttpResponseMessage> Add([HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            //string json = GetDocumentContents(req);

            dynamic dataArray = await req.Content.ReadAsAsync<object>();

            string output = dataArray.ToString();
           
            req.Properties[System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey] = new HttpConfiguration();

            if (HasGuidHeader(req) )
            {
                try
                {
                    Contact contact = JsonConvert.DeserializeObject<Contact>(output);
                    contact.AccessToken = Guid.NewGuid().ToString();
                    var result = contactRepository.AddContact(contact);

                    return req.CreateResponse(HttpStatusCode.OK, "Contact added");

                }
                catch (Exception)
                {

                    return req.CreateResponse(HttpStatusCode.Forbidden, "�"); ;
                }
            }
            else
            {
                return req.CreateResponse(HttpStatusCode.Forbidden, "�");
            }
        }


        [FunctionName("Update")]
        [TokenAuthenticationAttribute]
        public static async Task<HttpResponseMessage> Update([HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            req.Properties[System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey] = new HttpConfiguration();
            int id;
            bool idParam = int.TryParse(req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "id", true) == 0)
                .Value, out id);
            


            dynamic dataArray = await req.Content.ReadAsAsync<object>();

            string output = dataArray.ToString();
            if (HasGuidHeader(req) && idParam)
            {
                Contact contact = JsonConvert.DeserializeObject<Contact>(output);
                contact.AccessToken = req.Headers.GetValues("Token").FirstOrDefault();
                contact.Id = id;
                
                try
                {
                    var result = contactRepository.EditContact(contact);

                    return req.CreateResponse(HttpStatusCode.OK, "Contact updated");

                }
                catch (Exception)
                {

                    return req.CreateResponse(HttpStatusCode.Forbidden, "�"); ;
                }
            }
            else
            {
                return req.CreateResponse(HttpStatusCode.Forbidden, "�");
            }
        }

        [FunctionName("Delete")]
        [TokenAuthenticationAttribute]
        public static async Task<HttpResponseMessage> Delete([HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            req.Properties[System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey] = new HttpConfiguration();
            int id;
            bool idParam = int.TryParse(req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "id", true) == 0)
                .Value, out id);

            if (HasGuidHeader(req) && idParam)
            {
                
                try
                {
                    var result = contactRepository.DeleteContact(new Contact() { Id = id, AccessToken = req.Headers.GetValues("Token").FirstOrDefault() });
                    
                    return req.CreateResponse(HttpStatusCode.OK, "Contact deleted");

                }
                catch (Exception)
                {

                    return req.CreateResponse(HttpStatusCode.Forbidden, "�"); ;
                }
            }
            else
            {
                return req.CreateResponse(HttpStatusCode.Forbidden, "�");
            }
        }

        private static bool HasGuidHeader(HttpRequestMessage req)
        {
            IEnumerable<string> headerValues;

            var tokenHeaderValue = req.Headers.TryGetValues("Token", out headerValues);

           
            if (tokenHeaderValue == true && headerValues != null && headerValues.Count() > 0)
            {
                var token = headerValues.FirstOrDefault();

                Guid guid;
                bool isValid = Guid.TryParse(token, out guid);
                if (isValid && guid != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
