﻿using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Filters;

namespace FunctionAppTestApi.Attributes
{
    public class TokenAuthenticationAttribute : FunctionInvocationFilterAttribute
    {
        public override Task OnExecutingAsync(
        FunctionExecutingContext executingContext, CancellationToken cancellationToken)
        {
            var configuration = new HttpConfiguration();
            executingContext.Properties[System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey] = configuration;
            executingContext.Logger.LogInformation("WorkItemValidator executing...");

            

            return base.OnExecutingAsync(executingContext, cancellationToken);
        }

        
    }
}